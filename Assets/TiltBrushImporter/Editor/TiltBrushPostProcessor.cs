﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace OutOfBounds.TiltBrush
{
	public class TiltBrushPostProcessor : AssetPostprocessor
	{
		const string sketchPath = "TiltBrushSketches";

		static List<Material> brushMaterials = null;

		bool isImportingSketch = false;

		void OnPreprocessModel()
		{
			isImportingSketch = false;

			if (assetPath.Contains(sketchPath))
			{
				ModelImporter importer = assetImporter as ModelImporter;
				importer.importMaterials = true;

				isImportingSketch = true;
				LoadMaterials();
			}
		}

		void LoadMaterials()
		{
			brushMaterials = new List<Material>();

			string[] files = System.IO.Directory.GetFiles(Application.dataPath + "/TiltBrushImporter/Materials/");

			foreach (string file in files)
			{
				if (System.IO.Path.GetExtension(file) == ".mat")
				{
					string matName = System.IO.Path.GetFileName(file);
					string path = "Assets/TiltBrushImporter/Materials/" + matName;
					
					Material mat = AssetDatabase.LoadAssetAtPath<Material>(path);
				//	Debug.Log(mat.name);
					brushMaterials.Add(mat);
				}
			}
		}
		
		Material OnAssignMaterialModel(Material source, Renderer renderer)
		{
			if (!isImportingSketch)
				return null;

			string name = renderer.name.Replace("_geo", "");
			
			foreach (Material mat in brushMaterials)
			{
				if(mat.name.Contains(name))
				{
					Debug.Log("Found matching material for " + renderer.name);
					return mat;
				}
			}

			Debug.LogWarning("Did not find matching material for " + renderer.name);
			return null;
		}
	}
}