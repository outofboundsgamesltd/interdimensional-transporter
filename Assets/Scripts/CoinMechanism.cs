﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinMechanism : PlayerInteractable
{
	[SerializeField] new Light light;
	[SerializeField] float delay = 1f;

	void Awake()
	{
		light.enabled = false;
    }

	public override void Interact()
	{
		StartCoroutine(DoCoin());
	}
	
	public override void OnHoverEnter()
	{
		light.enabled = true;		
	}

	public override void OnHoverExit()
	{
		light.enabled = false;
	}

	IEnumerator DoCoin()
	{
		GetComponent<Collider>().enabled = false;

		GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(delay);

		GameManager.instance.GoToNextState();
		gameObject.SetActive(false);
	}
}
