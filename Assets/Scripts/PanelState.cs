﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PanelState
{
	InsertCoin,
	InputNumbers,
	Unresponsive
}
