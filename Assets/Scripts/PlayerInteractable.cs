﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerInteractable : MonoBehaviour
{
	public abstract void OnHoverEnter();
	public abstract void OnHoverExit();
	
	public abstract void Interact();
}
