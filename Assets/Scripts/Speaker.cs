﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speaker : OutOfBounds.SingletonBehaviour<Speaker>
{
	[SerializeField] AudioSource audioSource;

	public void PlayVO(AudioClip clip)
	{
		audioSource.Stop();
		audioSource.clip = clip;
		audioSource.Play();
	}

}
