﻿using UnityEngine;
using System.Collections;

namespace OutOfBounds.Math
{
    public static class RandomExtensions
    {
        public static float Next(this System.Random rng, float min, float max)
        {
            float result = (float)rng.NextDouble() * (max - min) + min;
			//Debug.Log("result " + result);

            return result;
        }
        
        public static float NextNormalizedFloat(this System.Random rng)
        {
            return (float)rng.NextDouble();
        }

    }
}