using UnityEngine;
using System.Collections;
using OutOfBounds.Math;

public static class AnimationCurveExtensions
{
	public static float lastTime(this AnimationCurve curve)
	{
		return curve[curve.length - 1].time;
	}	
	
	public static float EvaluateRandom(this AnimationCurve curve, System.Random rng)
	{
		return curve.Evaluate(rng.NextNormalizedFloat() * curve.lastTime());
	}
}
