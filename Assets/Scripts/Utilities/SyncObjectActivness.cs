﻿using UnityEngine;
using System.Collections;

namespace OutOfBounds.Utilities
{
	public class SyncObjectActivness : MonoBehaviour
	{
		[SerializeField] GameObject syncedObject;
				

		void OnEnable()
		{
			syncedObject.SetActive(true);
		}
		
		void OnDisable()
		{
			if(syncedObject != null) //object might have been destroyed before us
				syncedObject.SetActive(false);
		}
	}
}