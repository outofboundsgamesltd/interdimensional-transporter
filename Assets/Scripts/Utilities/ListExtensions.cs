﻿using UnityEngine;
using System.Collections.Generic;

namespace OutOfBounds.Math
{
    public static class ListExtensions
    {
        public static void FisherYatesShuffle<T>(this List<T> list, System.Random rng)
        {
            for(int i = list.Count - 1; i > 1; i--)
            {
                int j = rng.Next(0, i + 1); //have to plus one because random is not inclusive of highest value

                T temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
    }
}