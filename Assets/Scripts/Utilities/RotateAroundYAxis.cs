﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotateAroundYAxis : MonoBehaviour 
{
    public float rotateSpeed = 4f;

	// Update is called once per frame
	void Update ()  
    {
        transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
	}
}
