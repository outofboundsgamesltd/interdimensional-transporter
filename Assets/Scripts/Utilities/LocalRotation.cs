﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalRotation : MonoBehaviour
{
	[SerializeField] Vector3 angles;

	// Update is called once per frame
	void Update ()
	{
		transform.Rotate(angles * Time.deltaTime, Space.Self);		
	}
}
