﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : OutOfBounds.SingletonBehaviour<Panel>
{
	[SerializeField] TextMesh textMesh;
	
	public PanelState currentState = PanelState.InsertCoin;
	
	string currentString = "";

	public void SetText(string text)
	{
		currentString = text;
		textMesh.text = currentString;
	}

	public void SetState(PanelState state)
	{
		currentState = state;

		switch(currentState)
		{
			default:
				currentString = "";
				textMesh.text = currentString;
				break;
		}
	}
	
	public void InputDigit(string digit)
	{
		switch(currentState)
		{
			case PanelState.Unresponsive:
				break;
			case PanelState.InputNumbers:
				if(digit == "OK")
				{
					GameManager.instance.GoToNextState();
					currentState = PanelState.Unresponsive;
					print("going to next state");
					break;
				}
				currentString += digit;
				textMesh.text = currentString;
				break;
			default:
				break;
		}
	}
}
