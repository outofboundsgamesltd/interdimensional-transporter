﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reality : MonoBehaviour
{
	[SerializeField] new Camera camera;
	[SerializeField] Material skybox;
	[SerializeField] bool useFog = false;
	public Transform boxPivot;

	public void EnterReality()
	{
		camera.enabled = true;
		RenderSettings.skybox = skybox;
		RenderSettings.fog = useFog;

		gameObject.SetActive(true);
	}

	public void ExitReality()
	{
		camera.enabled = false;
		gameObject.SetActive(false);
	}

}
