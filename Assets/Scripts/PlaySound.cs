﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
	[SerializeField] AudioSource source;

	public void PlayTheSound()
	{
		source.Play();
	}
}
