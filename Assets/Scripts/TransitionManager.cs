﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class TransitionManager : OutOfBounds.SingletonBehaviour<TransitionManager>
{
	[SerializeField] ScreenOverlay screenOverlay;
	[SerializeField] Vortex vortex;
	[SerializeField] Twirl twirl;
	[SerializeField] BlurOptimized blur;

	[SerializeField] float vortexMaxAngle;
	[SerializeField] float twirlMaxAngle;
	[SerializeField] float blurMaxAmount = 1f;

	[SerializeField] AnimationCurve twirlCurve;
	[SerializeField] AnimationCurve transitionInCurve;
	[SerializeField] AnimationCurve transitionOutCurve;

	// Use this for initialization
	protected override void Awake()
	{
		base.Awake();

		screenOverlay.intensity = 0;
		vortex.angle = 0;
		GetComponent<Camera>().enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//if(Input.GetKeyDown(KeyCode.T))
		//{
		//	StartCoroutine(TransitionOut());
		//}				
	}

	public IEnumerator TransitionOut()
	{
		GetComponent<Camera>().enabled = true;
		GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
		GetComponent<AudioSource>().Play();

		for (float t = 0; t < transitionOutCurve.lastTime(); t += Time.deltaTime)
		{
			float a = t / transitionOutCurve.lastTime();
			screenOverlay.intensity = transitionOutCurve.Evaluate(t);
			vortex.angle = vortexMaxAngle * twirlCurve.Evaluate(a);
			twirl.angle = twirlMaxAngle * twirlCurve.Evaluate(a);
			blur.blurSize = blurMaxAmount * a;
			yield return 0;
		}

		screenOverlay.intensity = 1;
		vortex.angle = vortexMaxAngle * twirlCurve.Evaluate(1);
		twirl.angle = twirlMaxAngle * twirlCurve.Evaluate(1);
	}

	public IEnumerator TransitionIn()
	{
		for(float t = 0; t < transitionInCurve.lastTime(); t += Time.deltaTime)
		{
			float a = 1 - (t / transitionInCurve.lastTime());
			screenOverlay.intensity = transitionInCurve.Evaluate(t);
			vortex.angle = vortexMaxAngle * twirlCurve.Evaluate(a);
			twirl.angle = twirlMaxAngle * twirlCurve.Evaluate(a);
			blur.blurSize = blurMaxAmount * a;
			yield return 0;
		}

		screenOverlay.intensity = 0;
		vortex.angle = 0;

		GetComponent<Camera>().enabled = false;
	}

}
