﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealityManager : OutOfBounds.SingletonBehaviour<RealityManager>
{
	[SerializeField] Reality startReality;
	[SerializeField] Transform box;
	
	Reality currentReality;
	
	// Use this for initialization
	void Start ()
	{
		Reality[] realities = FindObjectsOfType<Reality>();

		foreach(Reality reality in realities)
		{
			reality.ExitReality();
		}

		SetReality(startReality);
    }
	
	public void SetReality(Reality newReality)
	{
		if (currentReality != null)
			currentReality.ExitReality();

		currentReality = newReality;
		currentReality.EnterReality();
		box.position = currentReality.boxPivot.position;
		box.rotation = currentReality.boxPivot.rotation;
	}
}
