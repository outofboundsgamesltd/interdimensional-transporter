﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{

	PlayerInteractable currentInteractable;

	void Update ()
	{
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hitInfo;
		Debug.DrawRay(ray.origin, ray.direction);

		if(Physics.Raycast(ray, out hitInfo, 99999))
		{
//			print(hitInfo.transform.name);

			PlayerInteractable interactable = hitInfo.transform.GetComponent<PlayerInteractable>();
			
			if(interactable != null)
			{
				if (currentInteractable != interactable)
				{
					if (currentInteractable != null)
						currentInteractable.OnHoverExit();

					interactable.OnHoverEnter();
				}

				if (Input.GetMouseButtonDown(0))
				{
					interactable.Interact();
				}
				
				currentInteractable = interactable;
			}
			else
			{
				if (currentInteractable != null)
				{
					currentInteractable.OnHoverExit();
					currentInteractable = null;
				}
			}
		}
		else
		{
			if (currentInteractable != null)
			{
				currentInteractable.OnHoverExit();
				currentInteractable = null;
			}
		}

	}
}
