﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PanelButton : PlayerInteractable
{
	[SerializeField] string digit;
	[SerializeField] new Light light;

	void Awake()
	{
		light.enabled = false;
	}

	public override void OnHoverEnter()
	{
		light.enabled = true;
	}

	public override void OnHoverExit()
	{
		light.enabled = false;
	}

	public override void Interact()
	{
		Panel.instance.InputDigit(digit);
		print("interacted with button");
		GetComponent<AudioSource>().Play();
	}
}
