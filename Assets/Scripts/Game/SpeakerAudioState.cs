﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeakerAudioState : GameState
{
	[SerializeField] AudioClip clip;
	[SerializeField] bool delayUntilFinished = true;
	[SerializeField] bool setPanelText = true;
	[SerializeField] string panelText = "";

	public override void EnterState()
	{
		StartCoroutine(PlayAudio());
	}

	IEnumerator PlayAudio()
	{
		Speaker.instance.PlayVO(clip);
		Panel.instance.SetText(panelText);

		if(delayUntilFinished)
			yield return new WaitForSeconds(clip.length);

		GameManager.instance.GoToNextState();
	}


}
