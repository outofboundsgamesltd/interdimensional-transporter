﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCreditsState : GameState
{
	[SerializeField] GameObject screen;

	public override void EnterState()
	{
		Credits.instance.SwitchScreen(screen);
		GameManager.instance.GoToNextState();
	}
}
