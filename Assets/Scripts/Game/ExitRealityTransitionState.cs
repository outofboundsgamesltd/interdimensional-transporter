﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitRealityTransitionState : GameState
{
	[SerializeField] bool waitUntilFinished = true;

	public override void EnterState()
	{
		StartCoroutine(DoTransition());
	}

	IEnumerator DoTransition()
	{
		if(waitUntilFinished)
			yield return StartCoroutine(TransitionManager.instance.TransitionIn());
		else
			StartCoroutine(TransitionManager.instance.TransitionIn());

		GameManager.instance.GoToNextState();
	}
}
