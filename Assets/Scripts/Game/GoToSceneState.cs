﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToSceneState : GameState
{
	[SerializeField] string sceneName = "IntroScene"	;

	public override void EnterState()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
	}
}
