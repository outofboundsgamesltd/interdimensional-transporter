﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterRealityTransitionState : GameState
{
	[SerializeField] Reality targetReality;
	[SerializeField] bool waitUntilFinished = true;

	public override void EnterState()
	{
		StartCoroutine(DoTransition());
	}

	IEnumerator DoTransition()
	{
		if(waitUntilFinished)
			yield return StartCoroutine(TransitionManager.instance.TransitionOut());
		else
			StartCoroutine(TransitionManager.instance.TransitionOut());

		RealityManager.instance.SetReality(targetReality);

		GameManager.instance.GoToNextState();
    }
}
