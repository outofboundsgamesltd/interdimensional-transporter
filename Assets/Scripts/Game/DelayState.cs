﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayState : GameState
{
	[SerializeField] float delay = 1f;

	public override void EnterState()
	{
		Invoke("AfterDelay", delay);
	}

	void AfterDelay()
	{
		GameManager.instance.GoToNextState();
	}
}
