﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : OutOfBounds.SingletonBehaviour<GameManager>
{
	[SerializeField] GameState[] states;
	

	[Header("Runtime")]
	[SerializeField] GameState currentState;
	[SerializeField] int index = 0;

	// Use this for initialization
	void Start ()
	{
		currentState = states[index];
		currentState.EnterState();	
	}
	
	public void GoToNextState()
	{
		index++;
		
		if (states.Length <= index)
			return;

		currentState = states[index];
		currentState.EnterState();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
			Time.timeScale = 0.5f;

		if (Input.GetKeyDown(KeyCode.Alpha2))
			Time.timeScale = 1f;

		if (Input.GetKeyDown(KeyCode.Alpha3))
			Time.timeScale = 2f;

		if (Input.GetKeyDown(KeyCode.Alpha4))
			Time.timeScale = 3f;

		if (Input.GetKeyDown(KeyCode.Alpha0))
			Time.timeScale = 20f;
	}

}
