﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPanelState : GameState
{
	public PanelState panelState;

	public override void EnterState()
	{
		Panel.instance.SetState(panelState);
		GameManager.instance.GoToNextState();
	}
}
