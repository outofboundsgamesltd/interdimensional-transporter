﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : OutOfBounds.SingletonBehaviour<Credits>
{
	public GameObject currentScreen;
	
	public void SwitchScreen(GameObject screen)
	{
		currentScreen.SetActive(false);
		currentScreen = screen;
		currentScreen.SetActive(true);
	}
}
